# Keycloak Single Sign On PoC

This is a small PoC showing Keycloak's single sign on feature.
It consists of two Quarkus Microservices registered as clients in Keycloak realm named _sso-poc_. These two services are confidential, meaning they are protected by Keycloak, requests without a valid access token will result in a 401. There is also a third, public client named curl and one predefined user. All realm regarded configuration will be imported at startup.

## Setup

### Install
* Java 11
* Maven 3.6+
* Docker 

### How to start Keycloak

There is a docker-compose file to easily start Keycloak.

    docker-compose up

### How to get an access token

Request a token from the following url with these credentials:

    curl -d 'client_id=curl' -d 'username=user' -d 'password=user' -d 'grant_type=password' 'http://localhost:8080/auth/realms/sso-poc/protocol/openid-connect/token'

### Request services

There are two services:

* greeting-services answering _Hello RESTEasy_ on localhost:8081/hello
* goodbye-service answering Goodbye RESTEasy_ on localhost:8082/goodbye

Start the services in their corresponding directory by:

    mvn quarkus:dev
    
Use the access token from step before to successfully request these endpoints.

    curl --location --request GET 'http://localhost:8081/hello' --header 'Authorization: Bearer <token>'
    curl --location --request GET 'http://localhost:8082/goodbye' --header 'Authorization: Bearer <token>'

### Logout and inspect session details

Visit http://localhost:8080/auth/admin/master/console/#/realms/sso-poc/users/c192f45f-0322-4111-861b-d35e1a078cd0/sessions to inspect the active sessions of the user, see which clients he requested within that session or log out.
